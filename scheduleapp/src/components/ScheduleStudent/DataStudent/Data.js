import React from 'react';
import PropTypes from 'prop-types';
import { Panel, Grid, Row, Col } from 'react-bootstrap';
const Data = ({cedula, regimen, nombres, indice, apellidos, nivel, cronograma}) => (
	
	<Grid xs={12} >
			<Row className="show-grid">
				<Panel>
					<Panel.Heading>
						<Grid>
							<Row className="show-grid">
								<Col xs={6} md={4}>
									<Row>{`Cédula: ${cedula}`}</Row>
									<Row>{`Apellidos: ${apellidos}`} </Row>
									<Row>{`Nombres: ${nombres}`} </Row>
								</Col>
								<Col xs={6} xsOffset={2}>
									{/*<Row>{'Sede'}</Row>
									<Row>{'Carrera'}</Row>*/}
									<Row>{`Ind. Acad ${indice}`}</Row>
									<Row>{`Nivel ${nivel}`}</Row>
								</Col>
							</Row>
						</Grid>
					</Panel.Heading>
					<Panel.Body>
						<Grid>
							<Row className="show-grid">
								<Col xs={12}><Panel.Title>CRONOGRAMA</Panel.Title></Col>
								<Col xs={12}><small>Su fecha y hora de inscripción es la siguiente:</small></Col>
								<Col xs={12}>{`${cronograma}`}</Col>
							</Row>
						</Grid>
					</Panel.Body>
				</Panel>
		</Row>
	</Grid>

);
Data.propTypes = {
	cedula: PropTypes.string.isRequired,
	regimen: PropTypes.string.isRequired,
	nombres: PropTypes.string.isRequired,
	indice: PropTypes.number.isRequired,
	apellidos: PropTypes.string.isRequired,
	nivel: PropTypes.number.isRequired,
}
export default Data;