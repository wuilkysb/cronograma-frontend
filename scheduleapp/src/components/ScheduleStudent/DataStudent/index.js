import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Data from './Data';

class DataStudent  extends React.Component{

	constructor(props){
		super(props)
		this.state = {
			cedula: "",
			regimen: false,
			nombres: "",
			indice: 0.0,
			apellidos: "",
			nivel: 0,
			cronograma: ""
		}
	}

	componentDidMount(){
		let cedula = 28428701
		let path = `http://localhost:3001/api/estudiantes/${cedula}`
		axios.get(path)
		.then(data => data.data)
		.then(data => {
			this.setState({
				cedula: data.idcard,
				regimen: data.regimen,
				nombres: data.name,
				indice: data.acaindex,
				apellidos: data.apellidos,
				nivel: data.level,
				cronograma: data.cronograma
			})
		})
	}

	render(){
		return(
			<Data cedula={this.state.cedula} regimen={this.state.regimen} nombres={this.state.nombres} indice={this.state.indice} apellidos={this.state.apellidos} nivel={this.state.nivel} cronograma={this.state.cronograma}/>
		);
	}
};
DataStudent.propTypes = {
	cedula: PropTypes.string.isRequired,
	regimen: PropTypes.string.isRequired,
	nombres: PropTypes.string.isRequired,
	indice: PropTypes.number.isRequired,
	apellidos: PropTypes.string.isRequired,
	nivel: PropTypes.number.isRequired,
}

export default DataStudent;